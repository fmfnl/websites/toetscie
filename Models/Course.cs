namespace FMF.Toetscie.Models;

public class Course
{
    public string? Name { get; set; }
    public List<Exam> Exams { get; set; } = new();
    public List<Note> Notes { get; set; } = new();
    public string? Route { get; set; }
    public string? DegreeName { get; set; }
}
