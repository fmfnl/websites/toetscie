using System.Globalization;
using System.Text.RegularExpressions;
using FMF.Toetscie.Models;
using FMF.Toetscie.Options;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Options;

namespace FMF.Toetscie.Repositories;

/*
    This repo wants filenames to be in the format [yyyy-MM-dd]_[type]_[author/lecturer name].pdf
    Examples: 
    2022-12-13_Exam + solutions_Diederik Roest.pdf
    2023-01-01_notes_Julian Gerritsen.pdf
    
    Date: Not optional, but if what you put can't be parsed as date, it will display that the date is unknown.
    
    Type: Not optional
    Magic types: "solutions", "notes" (case insensitive).
    If its type (see what "type" is in the naming convention above) is called "solutions" (Optionally with a digit at the end), 
    it will be listed under the exam with the same date and course.
    If its type is called "notes", it will be listed under the notes of the course. Instead of the lecturer name,
    put the name of the author/student that made it.
    In the case of multiple files on the same day, put a digit at the end of the type. Use this same digit for the corresponding solutions.
    "2022-12-13_Exam 2_Diederik Roest.pdf" will match "2022-12-13_Solutions 2_Diederik Roest.pdf" if there are multiple
    exams and solutions on the same day.
    
    If it's anything else, it will be listed like an exam, and the front end will show the exact type that you gave it.
    So the type could be something like "Test 3" or "midterm" and it will show that as well.
    
    Author: Optional
    
    If and only if the type is "notes" you can optionally add a title by using the format:
    "[yyyy-MM-dd]_[type]_[author/lecturer name]_[title].pdf"
    For example:
    2023-01-01_notes_Julian Gerritsen_Chapter 1.pdf
    
    You can append another _ to the file name (before the extension) and everything after that will be ignored.
*/

public class FileRepository : IFileRepository
{
    private readonly FileRepositoryOptions _options;
    private readonly ILogger _logger;

    public FileRepository(IOptions<FileRepositoryOptions> options, ILogger<IFileRepository> logger)
    {
        _logger = logger;
        _options = options.Value;

        if (_options.FilesFolder == string.Empty || !new DirectoryInfo(_options.FilesFolder).Exists)
        {
            // This is automatically logged as the following exception is not caught anywhere
            throw new DirectoryNotFoundException($"FilesFolder {_options.FilesFolder} from app settings not found");
        }
    }

    // The other List functions are private but this one is used in the controller 
    // since the index page lists degrees and is not part of another model.
    public async Task<List<Degree>> ListDegreesAsync()
        => await Task.Run(() =>
            Directory
                .EnumerateDirectories(_options.FilesFolder)
                .Select(path => new Degree
                {
                    Name = Path.GetFileName(path),
                    Route = Path.GetRelativePath(_options.FilesFolder, path)
                })
                .OrderBy(degree => degree.Name)
                .ToList()
        );

    public async Task<Degree> GetDegreeAsync(string degreeName)
        => await Task.Run(() =>
            new Degree
            {
                Name = degreeName,
                Route = degreeName,
                Courses = ListCourses(degreeName)
            }
        );

    public async Task<Course> GetCourseAsync(string degreeName, string courseName)
        => await Task.Run(() =>
        {
            // This list of IFiles will have types: Exam, Note, Solution inside of it. Those are all implementations of IFile.
            var files = ListFiles(degreeName, courseName);
            var exams = files.OfType<Exam>().ToList(); // List because we add things below.

            // Put the solutions under the right exams
            foreach (var sol in files.OfType<Solution>())
            {
                var examsOfDate = exams.FindAll(e => e.Date == sol.Date);
                var examMatch = examsOfDate.Count switch
                {
                    > 1 => FindExamBySolutionNumber(sol, examsOfDate), // Multiple exams on the same date
                    1 => examsOfDate[0],
                    _ => null
                };

                if (examMatch is not null)
                {
                    examMatch.Solutions = sol;
                }
                else
                {
                    exams.Add(new Exam
                    {
                        Date = sol.Date,
                        Solutions = sol
                    }); // Here we just give up and list it under a route-less exam.
                }
            }

            return new Course
            {
                Name = courseName,
                Route = Path.Combine(degreeName, courseName),
                Exams = exams.OrderByDescending(e => e.Date).ToList(),
                Notes = files.OfType<Note>().OrderByDescending(e => e.Date).ToList(),
                DegreeName = degreeName
            };
        });

    public async Task<(FileStream, string)> GetFileStreamAsync(string degreeName, string courseName, string fileName)
        => await Task.Run(() =>
            HandlePath(
                Path.Combine(_options.FilesFolder, degreeName, courseName, fileName), // fileDir
                fileDir =>
                {
                    new FileExtensionContentTypeProvider().TryGetContentType(fileDir, out var contentType);
                    return (new FileStream(fileDir, FileMode.Open),
                        contentType ?? "application/pdf");
                }
            )
        );

    private List<Course> ListCourses(string degreeName)
        => HandlePath(
            Path.Combine(_options.FilesFolder, degreeName), // degreeDir
            degreeDir =>
                Directory
                    .EnumerateDirectories(degreeDir)
                    .Select(path => new Course
                    {
                        Name = Path.GetFileName(path),
                        Route = Path.GetRelativePath(_options.FilesFolder, path)
                    })
                    .OrderBy(course => course.Name)
                    .ToList()
        );


    private List<IFile?> ListFiles(string degreeName, string courseName)
        => HandlePath(
            Path.Combine(_options.FilesFolder, degreeName, courseName), // courseDir
            courseDir =>
                Directory
                    .EnumerateFiles(courseDir)
                    .Select(GetFile) // Already ordered here
                    .ToList()
        );

    private IFile? GetFile(string path)
    {
        var attributes = Path.GetFileNameWithoutExtension(path).Split("_");
        if (attributes.Length < 2) return null;

        var typeLower = attributes[1].ToLower();
        // For solutions, either exactly match or exactly match with a digit behind it (possibly with a non-digit char in between).
        // For example, "solutions 1". "solution1" etc.
        // This way we won't match stuff like "Exam + solutions" as solutions.
        IFile file = Regex.Match(typeLower, "^solutions(\\D?\\d)?$").Success
            ? new Solution()
            : typeLower switch
            {
                "notes" => new Note { Title = attributes.ElementAtOrDefault(3) },
                _ => new Exam()
            };

        var dateParsed =
            DateTime.TryParseExact(attributes[0], "yyyy-MM-dd", CultureInfo.InvariantCulture, 0, out var date);
        file.Date = dateParsed ? date : null;
        file.Type = attributes[1];
        file.Author = attributes.ElementAtOrDefault(2);
        file.Route = Path.GetRelativePath(_options.FilesFolder, path);

        return file;
    }

    private static Exam? FindExamBySolutionNumber(Solution solution, IEnumerable<Exam> exams)
    {
        const string regexPattern = "(\\d)$"; // To extract any digit at the end of a string (capture group 1).
        var solNum = Regex.Match(solution.Type, regexPattern).Groups[1].Value;

        // Find an exam without solutions with the same digit at end of the type.
        return exams.FirstOrDefault(exam =>
            !exam.Type.Contains("solution") &&
            Regex.Match(exam.Type, regexPattern).Groups[1].Value == solNum);
    }

    private T HandlePath<T>(string dir, Func<string, T> callback)
    {
        // Check if we're still inside the exams folder
        if (Path.GetRelativePath(_options.FilesFolder, dir).StartsWith(".."))
        {
            Exception newEx = new ArgumentException($"Invalid file path: {dir}");
            _logger.LogWarning(newEx, "Exception occurred");
            throw newEx;
        }

        try
        {
            return callback(dir); // Do whatever we want to do, which includes directory/file enumeration of the dir.
        }
        catch (IOException ex) when (ex is DirectoryNotFoundException or FileNotFoundException) // No logging needed
        {
            throw new KeyNotFoundException($"Object at at {dir} not found", ex);
        }
        catch (IOException ex)
        {
            Exception newEx = new ArgumentException($"Invalid file path: {dir}", ex);
            _logger.LogWarning(newEx, "Exception occurred");
            throw newEx;
        }
    }
}